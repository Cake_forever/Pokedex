//
//  listapokedex.swift
//  Pokedex
//
//  Created by COTEMIG on 04/08/22.
//

import UIKit
struct pokemon{
    var nome:String
    var tipo1:String
    var tipo2:String
    var imagen:String
    var cor:String
}
class listapokedex: UIViewController, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    var pokemons:[pokemon] = [
    pokemon(nome: "bulbassauro", tipo1: "grama", tipo2: "veneno", imagen:"Bulba" , cor: "verde"),
        pokemon(nome: "ivyssauro", tipo1: "grama", tipo2: "veneno", imagen:"ivysaur_png_722324" , cor: "verde"),
        pokemon(nome: "venussauro", tipo1: "grama", tipo2: "veneno", imagen:"venusaur_png_1459038" , cor: "verde"),
        pokemon(nome: "charmander", tipo1: "fogo", tipo2: "terra", imagen:"image" , cor: "vermelho"),
        pokemon(nome: "charmeleon", tipo1: "fogo", tipo2: "terra", imagen:"005" , cor: "vermelho")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? TableViewCell else {return UITableViewCell()}
        let pokemon = pokemons[indexPath.row]
        cell.nome.text = pokemon.nome
        cell.tipo1.text = pokemon.tipo1
        cell.tipo2.text = pokemon.tipo2
        cell.imagem.image = UIImage(named: pokemon.imagen)
        cell.contentView.backgroundColor = UIColor (named: pokemon.cor)
        return cell
        
    }

}
